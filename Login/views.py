from django.shortcuts import render
from django.http import JsonResponse

def index(request):
	return render(request, 'Login.html')

def logout(request):
    request.session.flush()
    return JsonResponse({"logout": "success"})