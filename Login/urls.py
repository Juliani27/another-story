from django.conf.urls import url
from .views import index, logout

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^logout', logout, name='logout'),
]