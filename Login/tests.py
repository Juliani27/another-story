from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
import json
from django.contrib.auth.models import User

# Create your tests here.
class LoginUnitTest(TestCase):
	def test_login_url_is_exist(self):
		response = Client().get('/Login/')
		self.assertEqual(response.status_code, 200)

	def test_login_using_index_func(self):
		found = resolve('/Login/')
		self.assertEqual(found.func, index)

	def test_logout(self):
		user = User.objects.create(username='Riani')
		user.set_password('Riani2712')
		user.save()
		login = self.client.login(username='Riani', password='Riani2712')

		response = self.client.post('/Login/logout')
		self.assertEqual(json.loads(response.content), {"logout": "success"})