from django.shortcuts import render
from django.http import JsonResponse
import json
from django.urls import reverse
from django.http import HttpResponseRedirect
import requests

response = {}
def index(request):
	fav_book = request.session.get('fav_book', [])
	response["fav_book"] = fav_book
	response["num_fav"] = len(fav_book)
	return render(request, 'Books.html', response)

def checkUser(request):
	login = "false";
	if request.user.is_authenticated:
		login = "true";
	return JsonResponse({"login": login})

def getJSON(request):
	bookType = request.GET.get('type', '')
	url = 'https://www.googleapis.com/books/v1/volumes?q=' + bookType
	fav_book = request.session.get('fav_book', [])
	json_data = json.loads(requests.get(url).text)
	return JsonResponse({"data": json_data, "fav_book": fav_book})

def addFavorite(request):
	bookId = request.GET.get('id', '')
	fav_book = request.session.get('fav_book', [bookId])
	if bookId not in fav_book:
		fav_book.append(bookId)
	request.session['fav_book'] = fav_book
	return JsonResponse({"num_fav": len(fav_book)})

def deleteFavorite(request):
	bookId = request.GET.get('id', '')
	fav_book = request.session['fav_book']
	fav_book.remove(bookId)
	request.session['fav_num'] = fav_book
	return JsonResponse({"num_fav": len(fav_book)})
