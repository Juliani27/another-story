from django.conf.urls import url
from django.urls import path
from .views import index, checkUser, getJSON, addFavorite, deleteFavorite
#url for app

app_name = 'Books'
urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^getJSON', getJSON, name='getJSON'),
    url(r'^checkUser', checkUser, name='checkUser'),
	url(r'^addFavorite', addFavorite, name='addFavorite'),
	url(r'^deleteFavorite', deleteFavorite, name='deleteFavorite'),
]