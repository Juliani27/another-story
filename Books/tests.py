from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from .views import index
from django.contrib.auth.models import User
import json
import requests

class BooksUnitTest(TestCase):
	def test_books_url_is_exist(self):
		response = Client().get('/Books/')
		self.assertEqual(response.status_code, 200)

	def test_books_using_index_func(self):
		found = resolve('/Books/')
		self.assertEqual(found.func, index)

	def test_user_is_authenticated(self):
		response = Client().get('/Books/checkUser')
		self.assertEqual(json.loads(response.content), {"login": "false"})

		user = User.objects.create(username='Riani')
		user.set_password('Riani2712')
		user.save()
		login = self.client.login(username='Riani', password='Riani2712')

		response = self.client.post('/Books/checkUser')
		self.assertEqual(json.loads(response.content), {"login": "true"})

	def test_get_books_json(self):
		url = reverse('Books:getJSON')
		response = self.client.get(url + f'?type={"semogabukutipeinigaadabiarjsonnyagakpanjang"}')
		self.assertEqual(json.loads(response.content), {"data": {"kind": "books#volumes", "totalItems": 0}, "fav_book": []})

	def test_add_and_delete_favorite(self):
		url = reverse('Books:addFavorite')
		response = self.client.post(url + f'?id={"aidi"}')
		self.assertEqual(json.loads(response.content), {"num_fav": 1})

		response = self.client.post(url + f'?id={"aidi"}')
		self.assertEqual(json.loads(response.content), {"num_fav": 1})

		response = self.client.post(url + f'?id={"aidi2"}')
		self.assertEqual(json.loads(response.content), {"num_fav": 2})

		url = reverse('Books:deleteFavorite')
		response = self.client.post(url + f'?id={"aidi"}')
		self.assertEqual(json.loads(response.content), {"num_fav": 1})
