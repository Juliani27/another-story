from django.conf.urls import url
from .views import index, saveSubscriber, checkEmail, showSubscriber, checkPassword, unsubscribe

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^saveSubscriber', saveSubscriber, name='saveSubscriber'),
    url(r'^checkEmail', checkEmail, name='checkEmail'),
    url(r'^showSubscriber', showSubscriber, name='showSubscriber'),
    url(r'^checkPassword', checkPassword, name='checkPassword'),
    url(r'^unsubscribe', unsubscribe, name='unsubscribe'),
]