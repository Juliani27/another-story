from django.shortcuts import render
from .models import subscriberModel
from .forms import subscriberForm
from django.core.exceptions import ValidationError
from django.core.validators import validate_email
from django.http import JsonResponse

def index(request):
	response = {}
	subscribers = subscriberModel.objects.all()
	response['subscribers'] = subscribers
	response['form'] = subscriberForm
	return render(request, 'Subscriber.html', response)

def saveSubscriber(request):
	form = subscriberForm(request.POST)
	if form.is_valid():
		form.save()
		return JsonResponse(
			{'success': True}
		)
	response = JsonResponse(
		{'success': False, 'errors': form.errors}
	)
	response.status_code = 403
	return response

def checkEmail(request):
    email = request.GET.get('email', '')
    response = {'valid' : None, 'exists': None, 'messages': []}
    try:
        validate_email(email)
    except ValidationError as error:
        response['valid'] = False
        response['messages'] = error.messages
    else:
        response['valid'] = True
        if subscriberModel.objects.filter(email=email):
            response['exists'] = True
            response['messages'] = "This email has already been registered"
        else:
            response['exists'] = False
    return JsonResponse(response)

def showSubscriber(request):
	subscribers = list(subscriberModel.objects.values("name", "email", "password").order_by("name"))
	return JsonResponse({"subscribers": subscribers})

def checkPassword(request):
	email = request.GET.get('email', '')
	subscriber = subscriberModel.objects.filter(email=email)
	password = list(subscriber.values("password"))
	return JsonResponse({"password": password})

def unsubscribe(request):
	email = request.GET.get('email', '')
	response = {'success': True}
	subscriber = subscriberModel.objects.filter(email=email)
	subscriber.delete()
	return JsonResponse(response)