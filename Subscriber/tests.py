from django.test import TestCase
from django.test import Client
from django.urls import resolve, reverse
from django.http import HttpRequest
from .views import index
from .models import subscriberModel
from .forms import subscriberForm
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.chrome.options import Options
import time
import json

# Create your tests here.
class subscriberUnitTest(TestCase):
	def test_subscriber_url_is_exist(self):
		response = Client().get('/Subscribe/')
		self.assertEqual(response.status_code, 200)

	def test_subscriber_using_index_func(self):
		found = resolve('/Subscribe/')
		self.assertEqual(found.func, index)

	def test_index_contains_Subscribe_Newsletter(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Subscribe Newsletter', html_response)

	def test_model_can_create_new_subscriber(self):
		member = subscriberModel.objects.create(name = 'Riani', email = 'rianiakane@gmail.com',  password = 'Riani2712')
		count_all_member = subscriberModel.objects.all().count()
		self.assertEqual(count_all_member, 1)

	def test_email_is_not_valid(self):
		url = reverse('Subscriber:checkEmail')
		response = self.client.get(url + f'?email={"rianiakane"}')
		self.assertEqual(json.loads(response.content), {'valid': False, 'exists': None, 'messages': ['Enter a valid email address.']})

	def test_email_is_valid(self):
		url = reverse('Subscriber:checkEmail')
		response = self.client.get(url + f'?email={"rianiakane@gmail.com"}')
		self.assertEqual(json.loads(response.content), {'valid': True, 'exists': False, 'messages': []})

	def test_email_is_already_been_registered(self):
		url = reverse('Subscriber:checkEmail')
		subscriber = subscriberModel.objects.create(name="Riani", email="rianiakane@gmail.com", password="Riani2712")
		response = self.client.get(url + f'?email={"rianiakane@gmail.com"}')
		self.assertEqual(json.loads(response.content), {'valid': True, 'exists': True, 'messages': "This email has already been registered"})

	def test_form_is_invalid(self):
		response = Client().post('/Subscribe/saveSubscriber', {'name':'', 'email':'', 'password':'', 'passwordAgain':''})
		self.assertEqual(response.status_code, 403)
		self.assertEqual(subscriberModel.objects.all().count(), 0)

	def test_form_is_valid(self):
		response = Client().post('/Subscribe/saveSubscriber', {'name':'Riani', 'email':'rianiakane@gmail.com', 'password':'Riani2712', 
			'passwordAgain':'Riani2712'})
		self.assertEqual(response.status_code, 200)
		self.assertEqual(subscriberModel.objects.all().count(), 1)

	def test_list_subscribers(self):
		subscriber = subscriberModel.objects.create(name="Riani", email="rianiakane@gmail.com", password="Riani2712")
		response = Client().get('/Subscribe/showSubscriber')
		self.assertEqual(json.loads(response.content), {"subscribers": [{"name": "Riani", 
			"email": "rianiakane@gmail.com", "password": "Riani2712"}]})

	def test_check_password(self):
		url = reverse('Subscriber:checkPassword')
		subscriber = subscriberModel.objects.create(name="Riani", email="rianiakane@gmail.com", password="Riani2712")
		response = self.client.get(url + f'?email={"rianiakane@gmail.com"}')
		self.assertEqual(json.loads(response.content), {'password': [{'password': 'Riani2712'}]})

	def test_unsubscribe(self):
		subscriber = subscriberModel.objects.create(name="Riani", email="rianiakane@gmail.com", password="Riani2712")
		self.assertEqual(subscriberModel.objects.all().count(), 1)

		url = reverse('Subscriber:unsubscribe')
		response = self.client.get(url + f'?email={"rianiakane@gmail.com"}')
		self.assertEqual(subscriberModel.objects.all().count(), 0)