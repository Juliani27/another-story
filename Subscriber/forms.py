from django import forms
from .models import subscriberModel
import datetime

class subscriberForm(forms.ModelForm):
    class Meta:
        model = subscriberModel
        fields = '__all__'
        fields_required = '__all__'
        widgets = {
            'name': forms.TextInput(attrs={'class': 'form-control', 'id': 'subscriberName', 'placeholder': "Name"}),
            'email': forms.EmailInput(attrs={'class': 'form-control', 'id': 'subscriberEmail', 'placeholder': "Email Address"}),
            'password': forms.PasswordInput(attrs={'class': 'form-control', 'id': 'subscriberPassword', 
                'placeholder': "Password, minimum length of 8 alphanumeric characters"}),
        }

    passwordAgain = forms.CharField(label = '', widget = forms.PasswordInput(attrs={'class': 'form-control', 
        'id': 'subscriberPassword2', 'placeholder': "Confirm Password"}))