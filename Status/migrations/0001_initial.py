# Generated by Django 2.1.1 on 2018-10-10 23:25

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Status',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('new_status', models.CharField(max_length=300, verbose_name='Post Something')),
                ('date', models.DateField(default=datetime.date(2018, 10, 10))),
                ('time', models.TimeField(default=datetime.time(23, 25, 18, 400953))),
            ],
        ),
    ]
