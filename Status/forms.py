from django import forms
from .models import Status

class Status_Form(forms.ModelForm):
	class Meta: 
		model = Status
		fields = ['new_status']
		fields_required = ['new_status']
		widgets = {
			'new_status': forms.Textarea(attrs={'class': 'form-control', 'placeholder': "What's on your mind?"})
		}