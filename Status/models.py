from django.db import models
from django.utils import timezone

class Status(models.Model):
	new_status = models.CharField(max_length = 300, verbose_name="Post Something")
	date = models.DateTimeField(default=timezone.now)