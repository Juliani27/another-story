from django.test import TestCase, Client
from django.urls import resolve
from .views import index, saveStatus
from .models import Status
from .forms import Status_Form
from django.http import HttpRequest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.chrome.options import Options
import time
	
# Create your tests here.
class StatusUnitTest(TestCase):
	def test_status_url_is_exist(self):
		response = Client().get('/')
		self.assertEqual(response.status_code, 200)

	def test_status_using_index_func(self):
		found = resolve('/')
		self.assertEqual(found.func, index)

	def test_index_contains_Hello_Apa_Kabar(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Hello, Apa Kabar?', html_response)

	def test_model_can_create_new_status(self):
		new_status = Status.objects.create(new_status='Lagi ngerjain story 6 ppw nih')
		counting_all_status = Status.objects.all().count()
		self.assertEqual(counting_all_status, 1)

	def test_status_form_input_has_placeholder_and_css_classes(self):
		form = Status_Form()
		self.assertIn('class="form-control"', form.as_p())
		self.assertIn('placeholder="What&#39;s on your mind?"', form.as_p())

	def test_form_validation_for_blank_items(self):
		form = Status_Form(data = {'new_status': ''})
		self.assertFalse(form.is_valid())
		self.assertEqual(
			form.errors['new_status'],
			["This field is required."]
		)

	def test_Status_post_success_and_render_the_result(self):
		test = 'Ulalalalalala'
		response_post = Client().post('/saveStatus', {'new_status': test})
		self.assertEqual(response_post.status_code, 302)

		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertIn(test, html_response)

	def test_Status_post_error_and_render_the_result(self):
		test = 'Ulalalalalala' * 100
		response_post = Client().post('/saveStatus', {'new_status': test})
		self.assertEqual(response_post.status_code, 200)

		response= Client().get('/')
		html_response = response.content.decode('utf8')
		self.assertNotIn(test, html_response)

class StatusFunctionalTest(StaticLiveServerTestCase):
	def setUp(self):
		super(StatusFunctionalTest, self).setUp()
		chrome_options = Options()
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

	def tearDown(self):
		self.selenium.implicitly_wait(3)
		self.selenium.quit()
		super(StatusFunctionalTest, self).tearDown()

	def test_new_status_input(self):
		self.selenium.get('%s%s' % (self.live_server_url, '/'))
		self.assertIn('Add your first status!', self.selenium.page_source)

		#Add new status
		new_status = self.selenium.find_element_by_id('id_new_status')
		submit = self.selenium.find_element_by_id('submit')
		new_status.send_keys('Coba Coba')
		submit.send_keys(Keys.RETURN)
		self.assertIn('Coba Coba', self.selenium.page_source)
		time.sleep(5)

	#Layout Tests
	def test_status_title(self):
		self.selenium.get('%s%s' % (self.live_server_url, '/'))
		self.assertEqual('Status Juli!', self.selenium.title)
		time.sleep(5)

	def test_status_link_in_navBar(self):
		self.selenium.get('%s%s' % (self.live_server_url, '/'))
		navBar = self.selenium.find_element_by_class_name('nav-link').text
		self.assertIn('Status', navBar)
		time.sleep(5)

	#Style Tests
	def test_my_name_style_in_status_is_bold(self):
		self.selenium.get('%s%s' % (self.live_server_url, '/'))
		new_status = self.selenium.find_element_by_id('id_new_status')
		submit = self.selenium.find_element_by_id('submit')
		new_status.send_keys('Coba Coba')
		submit.send_keys(Keys.RETURN)

		myName = self.selenium.find_element_by_css_selector('p.Name').value_of_css_property('font-weight')
		self.assertEqual('700', myName)
		time.sleep(5)

	def test_date_style_in_status_is_float_right(self):
		self.selenium.get('%s%s' % (self.live_server_url, '/'))
		new_status = self.selenium.find_element_by_id('id_new_status')
		submit = self.selenium.find_element_by_id('submit')
		new_status.send_keys('Coba Coba')
		submit.send_keys(Keys.RETURN)

		date = self.selenium.find_element_by_css_selector('p.Date').value_of_css_property('float')
		self.assertEqual('right', date)
		time.sleep(5)

	def test_original_theme(self):
		self.selenium.get('%s%s' % (self.live_server_url, '/'))
		background = self.selenium.find_element_by_class_name('Hello').value_of_css_property('background-color')
		self.assertEqual('rgba(128, 0, 64, 1)', background)
		time.sleep(5)

	def test_change_theme(self):
		self.selenium.get('%s%s' % (self.live_server_url, '/'))
		themeButton = self.selenium.find_element_by_id('themeButton')
		themeButton.click()
		background = self.selenium.find_element_by_class_name('Hello').get_attribute("style")
		self.assertIn('rgb(206, 32, 41)', background)

		themeButton.click()
		background = self.selenium.find_element_by_class_name('Hello').get_attribute("style")
		self.assertIn('rgb(128, 0, 64)', background)
		time.sleep(5)
