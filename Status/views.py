from django.shortcuts import render
from django.http import HttpResponseRedirect
from .models import Status
from .forms import Status_Form

response = {}

def index(request):
	status = Status.objects.all().order_by("-date")
	response['status'] = status
	response['form'] = Status_Form
	return render(request, 'Status.html', response)

def saveStatus(request):
	form = Status_Form(request.POST or None)
	if (request.method == "POST" and form.is_valid()):
		response['new_status'] = request.POST['new_status']
		status = Status(new_status=response['new_status'])
		status.save()
		return HttpResponseRedirect('/')
	
	return render(request, 'Status.html', {'form':form})