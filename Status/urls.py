from django.conf.urls import url
from .views import index, saveStatus

urlpatterns = [
    url(r'^$', index, name='index'),
    url(r'^saveStatus', saveStatus, name='saveStatus'),
]
