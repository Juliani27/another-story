# Another PPW Story

## Maintainer
- Julia Ningrum : 1706979322

## Status Website
[![pipeline status](https://gitlab.com/Juliani27/another-story/badges/master/pipeline.svg)](https://gitlab.com/Juliani27/another-story/commits/master)
[![coverage report](https://gitlab.com/Juliani27/another-story/badges/master/coverage.svg)](https://gitlab.com/Juliani27/another-story/commits/master)

## Link Website Herokuapp
[http://status-juli.herokuapp.com/](http://status-juli.herokuapp.com/)