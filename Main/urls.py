from django.urls import path
from .views import index
#url for app

app_name = 'Main'
urlpatterns = [
    path('', index),
]