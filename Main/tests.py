from django.test import TestCase
from django.test import Client
from django.urls import resolve
from django.http import HttpRequest
from .views import index
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from django.contrib.staticfiles.testing import StaticLiveServerTestCase
from selenium.webdriver.chrome.options import Options
import time

class MainUnitTest(TestCase):
	def test_main_url_is_exist(self):
		response = Client().get('/Main/')
		self.assertEqual(response.status_code, 200)

	def test_main_using_index_func(self):
		found = resolve('/Main/')
		self.assertEqual(found.func, index)

	def test_main_contains_my_name(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('Julia Ningrum', html_response)

	def test_main_contains_my_education(self):
		request = HttpRequest()
		response = index(request)
		html_response = response.content.decode('utf8')
		self.assertIn('University of Indonesia', html_response)

class MainFunctionalTest(StaticLiveServerTestCase):
	def setUp(self):
		super(MainFunctionalTest, self).setUp()
		chrome_options = Options()
		chrome_options.add_argument('--no-sandbox')
		chrome_options.add_argument('--headless')
		self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)

	def tearDown(self):
		self.selenium.implicitly_wait(3)
		self.selenium.quit()
		super(MainFunctionalTest, self).tearDown()

	def test_accordion(self):
		self.selenium.get('%s%s' % (self.live_server_url, '/Main/'))
		accordion = self.selenium.find_element_by_class_name('Accordion')
		panelDisplay = self.selenium.find_element_by_class_name('Panel').value_of_css_property('display')
		self.assertEqual('none', panelDisplay)
		accordion.click()
		panelDisplay = self.selenium.find_element_by_class_name('Panel').get_attribute("style")
		self.assertIn('display: block', panelDisplay)
		time.sleep(5)

	def test_original_theme(self):
		self.selenium.get('%s%s' % (self.live_server_url, '/Main/'))
		profileBackground = self.selenium.find_element_by_class_name('Profile').value_of_css_property('background-color')
		self.assertEqual('rgba(192, 108, 132, 1)', profileBackground)
		time.sleep(5)

	def test_change_theme(self):
		self.selenium.get('%s%s' % (self.live_server_url, '/Main/'))
		themeButton = self.selenium.find_element_by_id('themeButton')
		themeButton.click()
		profileBackground = self.selenium.find_element_by_class_name('Profile').get_attribute("style")
		self.assertIn('rgb(121, 53, 53);', profileBackground)

		themeButton.click()
		profileBackground = self.selenium.find_element_by_class_name('Profile').get_attribute("style")
		self.assertIn('rgb(192, 108, 132);', profileBackground)
		time.sleep(5)