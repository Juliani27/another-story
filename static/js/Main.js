function accordionToggle(object) {
	object.toggleClass("active").next().slideToggle();
}

function brownTheme() {
	//Main
	$(".BC-P").css("background-color", "#793535");
	$(".F-P").css("color", "#592727");
	$(".Accordion").hover(function(){
		$(this).css("background-color", "#592727");
	}, function(){
		$(this).css("background-color", "#793535");
	});
	$(".Int-Icon").css("background-color", "#c17171");
	$(".progress-bar").removeClass('progressPurple');
	$(".progress-bar").addClass('progressBrown');

	//Status
	$(".Hello").css("background-color", "#CE2029");
	$(".Status-Container").css("background-color", "#AD5049");

	//Book
	$('thead').css('background-color', "#793535");
}

function purpleTheme() {
	//Main
	$(".BC-P").css("background-color", "#C06C84");
	$(".F-P").css("color", "#B34D69");
	$(".Accordion").hover(function(){
		$(this).css("background-color", "#8F3D54");
	}, function(){
		$(this).css("background-color", "#C06C84");
	});
	$(".Int-Icon").css("background-color", "#E0B8C3");
	$(".progress-bar").removeClass('progressBrown');
	$(".progress-bar").addClass('progressPurple');

	//Status
	$(".Hello").css("background-color", "#800040");
	$(".Status-Container").css("background-color", "#a1455d");

	//Books
	$('thead').css('background-color', "#8F3D54");
}

$(document).ready(function() {
	  	$('#loader').css('display', 'none');
	  	$('.Page').css('display', 'block');

	if (!localStorage.theme) {localStorage.theme = "purpleTheme";}
	
	switch(localStorage.theme) {
		case "purpleTheme":
			purpleTheme();
			break;
		case "brownTheme":
			brownTheme();
			break;
	}

	$('.Accordion').click(function() {
		accordionToggle($(this));
	});

	$('#themeButton').click(function() {
		switch(localStorage.theme) {
			case "purpleTheme":
				localStorage.theme = "brownTheme";
				brownTheme();
				break;
			case "brownTheme":
				localStorage.theme = "purpleTheme";
				purpleTheme();
				break;
		}
	});

	$('#logoutLink').click(function() {
		$.ajax({
	        type        : 'GET',
	        url         : '/Login/logout',
	        dataType    : 'json',
	    }).then(function(response) {
	        window.location.replace("/");
	    });
	});
});