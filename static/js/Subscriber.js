var nameIsValid = false;
var emailIsValid = false;
var passwordIsValid = false;
var unsubEmail = "";

function validateForm() {
    if (!(nameIsValid && emailIsValid && passwordIsValid)) {
        $('#subscribeButton').attr('disabled', true);
    } else if (nameIsValid && emailIsValid && passwordIsValid) {
    	$('#subscribeButton').attr('disabled', false);
    }
}

function validateName(name) {
    if (name.length > 0) {
        nameIsValid = true;
        validateForm();
    } else {
        nameIsValid = false;
        validateForm();
    };
}

function validateEmail(email) {
    return $.ajax({
        type        : 'GET',
        url         : 'checkEmail?email=' + email,
        dataType    : 'json',
    }).then(function(response) {
        if (response.valid && !response.exists) {
        	$("#emailError").text("");
            emailIsValid = true;
            validateForm();
        } else {
            $("#emailError").text(response.messages);
            emailIsValid = false;
            validateForm();
        }
    });
}

function validatePassword(password, password2) {
    if (password.length < 8) {
        $("#passwordError").text("Password must be at least 8 characters long.");
        passwordIsValid = false;
        validateForm();
    } else if (!password.match(/^[0-9a-z]+$/i)) {
    	$("#passwordError").text("Password must only contains letters or numbers.");
        passwordIsValid = false;
        validateForm();
    } else if (password != password2) {
    	$("#passwordError").text("The two password fields don't match.");
        passwordIsValid = false;
        validateForm();
    } else {
        $("#passwordError").text("");
        passwordIsValid = true;
        validateForm();
    };
}

function checkPassword(password) {
	return $.ajax({
        type        : 'GET',
        url         : 'checkPassword?email=' + unsubEmail,
        dataType    : 'json',
    }).then(function(response) {
        if (response.password[0].password == password) {
        	unsubscribe();
        	$('#unsubscribeModal').modal('hide');
        	$(".modal-title").text("You are now Unsubscribed!");
			$(".modal-body-text").text("You no longer hear from us :(.");
			$('#templateModal').modal('show');
        	showSubscribers();
        	$("#unsubscribePassword").val("");
        } else {
        	$(".modal-body-text-2").text("Wrong Password!");
            $("#unsubscribePassword").val("");
        }
    });
}

function unsubscribe(argument) {
	return $.ajax({
        type        : 'GET',
        url         : 'unsubscribe?email=' + unsubEmail,
    })
}

function showSubscribers() {
	$('#subscriberTable').empty();
	$.ajax({
		url: "/Subscribe/showSubscriber",
		success: function(result){
		    $.each(result.subscribers, function(i, item) {
		        $('<tr>').append(
		        	$('<td class="align-middle P-20">').text(i+1),
		            $('<td class="align-middle P-20">').text(item.name),
		            $('<td class="align-middle P-20"> id="emailSub"').text(item.email),
		            $('<td class="align-middle P-20">').append('<button class="btn BC-P F-W" id="unsubscribeButton">unsubscribe</button>'),
		        ).appendTo('#subscriberTable');
		    });
		}
	});
}

$(document).ready(function(){
	validateForm();

	showSubscribers();

	$("tbody").delegate("#unsubscribeButton", "click", function(){
		$("#unsubscribePassword").val("");
		$(".modal-body-text-2").text("");
		$('#unsubscribeModal').modal('show');
		unsubEmail = ($(this).closest('tr').find('td:eq(2)').text());
	});

	$('#passwordConfirm').click(function() {
		checkPassword($("#unsubscribePassword").val());
	});

	$('form').submit(function(event){
		var formData = $(this).serializeArray();
		$.ajax({
			url: "saveSubscriber",
			type : "POST",
			dataType: 'json',
			data : formData,
			})
		.done(function(response){
			$(".modal-title").text("Thank You For Subscribing!");
			$(".modal-body-text").text("You're almost done! Please check your email to confirm your subscription.");
			$('#templateModal').modal('show');
			$(".form-control").val("");
			showSubscribers();
			nameIsValid = false;
			emailIsValid = false;
			passwordIsValid = false;
			validateForm();

		});
		event.preventDefault();
	});
	
	$("#subscriberName").on("input keyup paste click", function() {
		validateName($("#subscriberName").val());
	});

	$("#subscriberEmail").on("input keyup paste click", function() {
		validateEmail($("#subscriberEmail").val());
	});

	$("#subscriberPassword, #subscriberPassword2").on("input keyup paste click", function() {
		validatePassword($("#subscriberPassword").val(), $("#subscriberPassword2").val());
	});
});

