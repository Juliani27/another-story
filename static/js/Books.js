function addFavorite(object) {
	var id = object.attr('value');
	if (object.hasClass('Checked')) {
		object.removeClass('Checked');
		$.ajax({
	        type        : 'GET',
	        url         : 'deleteFavorite?id=' + id,
	        dataType    : 'json',
	    }).then(function(response) {
	        $(".Total-Star").text(response.num_fav);
	    });
	} else {
		object.addClass('Checked');
		$.ajax({
	        type        : 'GET',
	        url         : 'addFavorite?id=' + id,
	        dataType    : 'json',
	    }).then(function(response) {
	        $(".Total-Star").text(response.num_fav);
	    });
	}
}

$(document).ready(function(){
	$('#submitSearch').click(function(){
		$('#booksTable').empty();
		var bookType = $('#bookType').val()
		$.ajax({
			type        : 'GET',
	        url         : 'getJSON?type=' + bookType,
	        dataType    : 'json',
			success: function(result){
			    $.each(result.data.items, function(i, item) {
			    	if (result.fav_book.includes(item.id)) {
				        $('<tr>').append(
				        	$('<td class="align-middle">').text(i+1),
				            $('<td class="align-middle">').append('<img src=' + item.volumeInfo.imageLinks.smallThumbnail + "'/>"),
				            $('<td class="align-middle">').text(item.volumeInfo.title),
				            $('<td class="align-middle">').text(item.volumeInfo.authors),
				            $('<td class="align-middle">').text(item.volumeInfo.publisher),
				            $('<td class="align-middle">').text(item.volumeInfo.publishedDate),
				            $('<td class="align-middle">').append('<i class="fa fa-star Checked P-30" id="favoriteIcon" value="' + item.id + '"></i>'),
				        ).appendTo('#booksTable');
				    } else {
				        $('<tr>').append(
				        	$('<td class="align-middle">').text(i+1),
				            $('<td class="align-middle">').append('<img src=' + item.volumeInfo.imageLinks.smallThumbnail + "'/>"),
				            $('<td class="align-middle">').text(item.volumeInfo.title),
				            $('<td class="align-middle">').text(item.volumeInfo.authors),
				            $('<td class="align-middle">').text(item.volumeInfo.publisher),
				            $('<td class="align-middle">').text(item.volumeInfo.publishedDate),
				            $('<td class="align-middle">').append('<i class="fa fa-star P-30" id="favoriteIcon" value="' + item.id + '"></i>'),
				        ).appendTo('#booksTable');
				    }
			    });
			}
		});
	});

	$("tbody").delegate(".fa-star", "click", function(){
		var icon = $(this);
		$.ajax({
	        type        : 'GET',
	        url         : 'checkUser',
	        dataType    : 'json',
	    }).then(function(response) {
	        if (response.login === "true") {
	        	addFavorite(icon);
	        } else {
	        	$('#loginModal').modal('show');
	        }
	    });
	});

	$("#loginBtn").click(function(){
		window.location.replace("/Login");
	});
});
